class Ability
  include CanCan::Ability

  def initialize(user)
    can :manage, User, parent_id: user.id
    user.permissions.each do |permission|
      can permission.action.to_sym, (permission.resource.constantize rescue permission.resource.to_sym)
    end
  end
end
