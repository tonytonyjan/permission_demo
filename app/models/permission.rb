class Permission < ActiveRecord::Base
  include Permission::Policy
  belongs_to :user
  add_policy_with_resource Post, User
  add_policy 'kill man', 'drink water', 'manage all'
end
