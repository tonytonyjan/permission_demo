class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  belongs_to :parent, class_name: User
  has_many :children, class_name: User, foreign_key: :parent_id
  has_many :permissions, dependent: :destroy
  delegate :policy_exist?, to: :permissions

  def set_permission action, resource
    permissions.find_or_create_by! action: action, resource: resource
  end

  def parent?
    parent_id.nil?
  end

  def child?
    not parent?
  end

  def policies
    permissions.map{ |p| p.policy }
  end

  def policies= values
    self.permissions = values.map! do |v|
      action, resource = v.split
      Permission.new action: action, resource: resource
    end
  end
end
