require 'active_support/concern'

class Permission
  module Policy
    extend ActiveSupport::Concern

    class_methods do
      @@policies = []

      def policies
        @@policies
      end

      def policy_exist? policy
        action, resource = policy.split
        exists?(action: action, resource: resource)
      end

      def add_policy *policies
        @@policies |= policies
      end

      def add_policy_with_resource *models
        models.each do |model|
          @@policies |= %w[create read update destroy].map!{|action| "#{action} #{model}"}
        end
      end
    end
  end
end