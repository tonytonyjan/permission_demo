class UsersController < ApplicationController
  load_and_authorize_resource

  def index
    @users = User.all
  end

  def new
    @user = User.new
  end

  def create
    @user = if params[:button] == 'top_user'
      authorize! :manage, :all
      User.new user_params
    else
      current_user.children.new user_params
    end
    if @user.save
      redirect_to edit_user_path(@user), notice: '已新增！'
    else
      render :new
    end
  end

  def edit
  end

  def update
    if @user.update user_params
      redirect_to edit_user_path(@user), notice: '已更新！'
    else
      render :edit
    end
  end

  def destroy
    @user.destroy
    redirect_to User, notice: '刪除囉'
  end

private

  def set_user
    @user = User.find(params[:id])
  end

  def user_params
    params.require(:user).permit(:email, :password, :password_confirmation, policies: [])
  end
end
