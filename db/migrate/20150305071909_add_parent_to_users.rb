class AddParentToUsers < ActiveRecord::Migration
  def change
    add_reference :users, :parent, index: true
    add_foreign_key :users, :users, column: :parent_id
  end
end
