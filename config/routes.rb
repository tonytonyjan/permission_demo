Rails.application.routes.draw do
  root 'pages#home'
  devise_for :users, path: :account
  resources :posts, :users
end
